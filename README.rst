.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/osmroadextractor.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/osmroadextractor
    .. image:: https://readthedocs.org/projects/osmroadextractor/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://osmroadextractor.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/osmroadextractor/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/osmroadextractor
    .. image:: https://img.shields.io/pypi/v/osmroadextractor.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/osmroadextractor/
    .. image:: https://img.shields.io/conda/vn/conda-forge/osmroadextractor.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/osmroadextractor
    .. image:: https://pepy.tech/badge/osmroadextractor/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/osmroadextractor
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/osmroadextractor

.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

|

================
osmroadextractor
================


    Extract road from osm data


A longer description of your project goes here...


.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.1.4. For details and usage
information on PyScaffold see https://pyscaffold.org/.
